//
//  ViewController.m
//  CollectionViewDemo
//
//  Created by James Cash on 12-07-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"
#import "OurCell.h"

@interface ViewController () <UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

// we keep a strong reference to the default layout because we're going to switch away from it and then back; if this were weak, it would be deallocated when we switch away & then crash when we try to switch back
@property (strong, nonatomic) IBOutlet UICollectionViewFlowLayout *defaultLayout;

@property (strong,nonatomic) UICollectionViewFlowLayout *bigLayout;

@property (strong,nonatomic) UICollectionViewFlowLayout *middleLayout;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.bigLayout = [[UICollectionViewFlowLayout alloc] init];
    self.bigLayout.itemSize = CGSizeMake(200, 175);
    self.bigLayout.minimumInteritemSpacing = 35;
    self.bigLayout.minimumLineSpacing = 80;
    self.bigLayout.sectionInset = UIEdgeInsetsMake(30, 30, 30, 30);

    self.middleLayout = [[UICollectionViewFlowLayout alloc] init];
    self.middleLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    self.middleLayout.itemSize = CGSizeMake(125, 125);
    self.middleLayout.headerReferenceSize = CGSizeMake(75, 0);
    self.middleLayout.minimumInteritemSpacing = 10;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)toggleLayout:(id)sender {
    UICollectionViewLayout *next;
    if (self.collectionView.collectionViewLayout == self.bigLayout) {
        next = self.middleLayout;
    } else if (self.collectionView.collectionViewLayout == self.middleLayout) {
        next = self.defaultLayout;
    } else {
        next = self.bigLayout;
    }
    [self.collectionView.collectionViewLayout invalidateLayout];
    [self.collectionView setCollectionViewLayout:next animated:YES];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 8;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    // in practice, this would probably be something like self.myDataSource.count
    // just using the section number here to have an increasing number of items
    return 3 + section;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    OurCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ourCell" forIndexPath:indexPath];

    cell.cellLabel.text = [NSString stringWithFormat:@"%ld/%ld", indexPath.section, indexPath.item];

    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        UICollectionReusableView *header = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"ourHeader" forIndexPath:indexPath];

        UILabel *headerLabel = [header viewWithTag:1];
        headerLabel.text = [NSString stringWithFormat:@"Section %ld", indexPath.section];

        return header;
    }
    return nil;
}

@end
