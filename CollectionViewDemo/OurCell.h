//
//  OurCell.h
//  CollectionViewDemo
//
//  Created by James Cash on 12-07-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OurCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *cellLabel;

@end
